from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext_lazy as _


class City(MPTTModel):
    name = models.CharField(verbose_name=_('name'), max_length=255, blank=True, null=False)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')

    class MPTTMeta:
        order_insertion_by = ['name']
