from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator

from apps.users.models import Car


class Ride(models.Model):

    date = models.DateTimeField(
        verbose_name=_('date')
    )
    point_a = models.CharField(
        max_length=24, verbose_name=(_('Point A'))
    )
    point_b = models.CharField(
        max_length=24, verbose_name=(_('Point B'))
    )
    seat = models.PositiveIntegerField(
        validators=[MaxValueValidator(20)]
    )
    price = models.DecimalField(
        verbose_name=_('price'), max_digits=10, decimal_places=2, blank=True, null=True, default=Decimal("0.00")
    )

    driver = models.ForeignKey(
        'users.User', null=False, on_delete=models.CASCADE
    )
    vehicle = models.ForeignKey(
        Car, blank=True, null=True, on_delete=models.CASCADE
    )
    note = models.TextField(
        verbose_name=_('note'), null=True, blank=True
    )
    smoking_salon = models.BooleanField(
        default=False, verbose_name=_('smoking salon')
    )
    animals = models.BooleanField(
        default=False, verbose_name=_('animals')
    )
    kids_seat = models.BooleanField(
        default=False, verbose_name=_('kids seat')
    )

    class Meta:
        verbose_name = (_('Ride'))
        verbose_name_plural = (_('Rides'))

    def __str__(self):
        return f'{self.date} #{self.point_a} - {self.point_b} '

