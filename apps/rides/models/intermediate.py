from django.db import models
from django.utils.translation import ugettext_lazy as _


class Intermediate(models.Model):
    ride = models.ForeignKey(
        'Ride', related_name='intermediate', verbose_name=_('ride'), blank=True, null=True, on_delete=models.CASCADE
    )
    point = models.CharField(
        max_length=24,  verbose_name=_('Point')
    )

    class Meta:
        verbose_name = (_('Intermediate Point'))
        verbose_name_plural = (_('Intermediate Point'))

    def __str__(self):
        return f'{self.ride} - {self.point} '
