from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Passenger(models.Model):
    ride = models.ForeignKey(
        'Ride', related_name='passengers', verbose_name=_('ride'), on_delete=models.CASCADE, blank=True, null=True
    )
    user = models.ForeignKey(
        'users.User', related_name='passengers', verbose_name=_('user'), on_delete=models.CASCADE, blank=True, null=True
    )
    seat = models.PositiveIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(20)], default=0
    )

    def __str__(self):
        return f'{self.ride} - {self.user}'
