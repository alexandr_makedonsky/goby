from rest_framework import mixins, viewsets, permissions
from apps.rides.models import Ride, Passenger
from apps.rides.serializers import PassengerSerializer
from django_filters import rest_framework as rest_filters
from rest_framework import filters
from apps.rides import filters as ride_filters


class PassangerViewSet(
    mixins.ListModelMixin, viewsets.GenericViewSet
):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Passenger.objects.all()
    serializer_class = PassengerSerializer
