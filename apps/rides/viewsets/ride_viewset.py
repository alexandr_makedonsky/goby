from django.db.models import Q
from rest_framework import mixins, viewsets, permissions, status
from rest_framework.response import Response

from apps.rides.models import Ride
from apps.rides.serializers import RideSerializer, RideCreateSerializer
from django_filters import rest_framework as rest_filters
from rest_framework import filters
from apps.rides import filters as ride_filters


class RideUserViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    permission_classes = (permissions.IsAuthenticated,)
    serializers = {
        'list': RideSerializer,
        'create': RideCreateSerializer,
        'retrieve': RideSerializer,
        'update': RideSerializer,
    }
    queryset = Ride.objects.all()
    filter_backends = [rest_filters.DjangoFilterBackend, filters.SearchFilter]
    filterset_class = ride_filters.RideFilter
    filterset_fields = ['price', 'date', 'point_a', 'point_b']
    search_fields = ['date', 'point_a', 'point_b']
    http_method_names = ['get', 'post', 'put', 'delete', 'head', 'options', 'trace']

    def get_serializer_class(self):
        return self.serializers[self.action]

    def get_queryset(self, **kwargs):
        return self.queryset.filter(
            Q(driver=self.request.user) | Q(passengers__user=self.request.user)
              )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ride = serializer.save()

        serializer = RideSerializer(instance=ride, context=serializer.context)

        return Response(serializer.data, status=status.HTTP_200_OK)

