from rest_framework import mixins, viewsets, permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import filters

from apps.rides import serializers, models
from django_filters import rest_framework as rest_filters
from apps.rides import filters as ride_filters


class RidePublicViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet
):
    permission_classes = (permissions.IsAuthenticated,)
    serializers = {
        'list': serializers.RideSerializer,
        'retrieve': serializers.RideSerializer,
        'update': serializers.RideRentSerializer,

        'rent': serializers.RideRentSerializer,
        'reject': serializers.RideRejectRentSerializer,
    }
    queryset = models.Ride.objects.all()
    filter_backends = [rest_filters.DjangoFilterBackend, filters.SearchFilter]
    filterset_class = ride_filters.RideFilter
    filterset_fields = ['price', 'date', 'point_a', 'point_b']
    search_fields = ['date', 'point_a', 'point_b']
    http_method_names = ['get', 'put']

    def get_serializer_class(self):
        return self.serializers[self.action]

    @action(methods=['put'], detail=True)
    def rent(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        serializer = serializers.RideSerializer(instance=instance, context=serializer.context)

        return Response(serializer.data)

    @action(methods=['put'], detail=True)
    def reject(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        serializer = serializers.RideSerializer(instance=instance, context=serializer.context)

        return Response(serializer.data)

