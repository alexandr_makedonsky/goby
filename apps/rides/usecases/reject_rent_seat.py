from __future__ import annotations
from rest_framework.exceptions import ValidationError
from apps.users.models import User
from apps.rides.models import Ride, Passenger
from django.utils.translation import ugettext_lazy as _


class RejectRentSeatUseCase:

    def __call__(self, ride: Ride, user: User, seat: int) -> Ride:

        passenger = Passenger.objects.filter(
            ride=ride, user=user
        ).first()

        self.validate(passenger)

        passenger.seat -= seat
        passenger.save()

        if passenger.seat == 0:
            passenger.delete()

        ride.seat += seat
        ride.save()

        return ride

    def validate(self, passenger):

        is_valid = passenger is None
        if is_valid:
            raise ValidationError(detail={"seat": _(f'Seat not rented yet')})
