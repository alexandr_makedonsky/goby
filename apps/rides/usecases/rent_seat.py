from __future__ import annotations
from django.utils.translation import ugettext_lazy as _

import datetime
from rest_framework.exceptions import ValidationError
from apps.users.models import User
from apps.rides.models import Ride, Passenger


class RentSeatUseCase:

    def __call__(self, ride: Ride, user: User, seat: int) -> Ride:
        self.validate(ride, seat)

        passanger = Passenger.objects.create(
            ride=ride, user=user, seat=seat
        )
        ride.seat -= seat
        ride.save()
        return ride

    def validate(self, ride: Ride, seat: int):
        ride = Ride.objects.get(pk=ride.pk)
        if not ride:
            raise ValidationError(detail={"RIDE": _('Ride doesn\'t exist')})
        ride_seat = ride.seat
        if ride_seat < seat:
            raise ValidationError(detail={"seat": _(f'There\'re just {ride_seat} seat')})
