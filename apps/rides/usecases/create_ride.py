import re
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError
from apps.rides.models import Ride
from apps.users.models import User
from datetime import datetime
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError


class RideCreateUseCase:
    def __call__(
            self,
            driver: User,
            date: datetime,
            point_a: str,
            point_b: str,
            price: Decimal,
            seat: int,
            note: str,
            smoking_salon: bool,
            animals: bool,
            kids_seat: bool,
    ) -> Ride:

        ride = Ride.objects.create(
            driver=driver,
            date=date,
            point_a=point_a,
            point_b=point_b,
            price=price,
            seat=seat,
            note=note,

            smoking_salon=smoking_salon,
            animals=animals,
            kids_seat=kids_seat
        )

        return ride
