from django.contrib import admin
from apps.rides.models import Ride, City,Passenger, Intermediate
from mptt.admin import MPTTModelAdmin


admin.site.register(Ride)
admin.site.register(Passenger)
admin.site.register(Intermediate)


class CityMPTTModelAdmin(MPTTModelAdmin):
    mptt_level_indent = 20
    list_display = ('name', 'parent')
    fields = ('name', 'parent')


admin.site.register(City, CityMPTTModelAdmin)
