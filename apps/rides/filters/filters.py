from django import forms
from django_filters import rest_framework as filters
from apps.rides.models import Ride


class RideFilter(filters.FilterSet):
    point_a = filters.CharFilter(lookup_expr='iexact')
    point_b = filters.CharFilter(lookup_expr='iexact')

    date = filters.DateTimeFilter(field_name='date', lookup_expr='lte')

    class Meta:
        model = Ride
        fields = [
            'price',
            'point_a',
            'point_b',
            'date',

        ]
