from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.rides import viewsets


router = DefaultRouter()
router.register('rides', viewsets.RidePublicViewSet, basename="RidePublicViewSet")

user_rides_router = DefaultRouter()
user_rides_router.register('rides', viewsets.RideUserViewSet, basename="RideUserViewSet")


urlpatterns = [
    path('', include(router.urls)),
    path('users/', include(user_rides_router.urls)),
]
