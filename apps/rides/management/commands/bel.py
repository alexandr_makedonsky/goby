from django.core.management.base import BaseCommand
import csv
from apps.rides.models import City
from collections import namedtuple


CityNode = namedtuple('CityNode', ('name', 'children'))


class Command(BaseCommand):
    help = 'Displays cities of Belarus'

    def handle(self, *args, **kwargs):
        def create_tree():
            path = './cities/by-list.csv'
            with open(path) as f:
                reader = csv.DictReader(f)
                cities = {}
                for row in reader:
                    region = row['Область'].strip()
                    name = (row.get('Город') or region).strip()

                    city = CityNode(name, [])
                    parent = cities.get(region)
                    # import pdb; pdb.set_trace()
                    if not parent:
                        cities[region] = city
                    else:
                        parent.children.append(city)
                return list(cities.values())

        def create_category(parent: City, node: CityNode):
                cit = City.objects.create(
                    parent=parent, name=node.name
                )
                print(f'add city "{cit.name}" to "{parent.name}"')
                for child in node.children:
                    create_category(cit, child)

        root = City.objects.create(name='root', parent=None)
        cities = create_tree()
        for city in cities:
            create_category(root, city)



