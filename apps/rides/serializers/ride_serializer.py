from django.utils import timezone

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.rides.models import Ride
from apps.rides import usecases
from apps.users.serializers import UserSerializer
from .intermediate_serializer import IntermediateSerializer
from .passenger_serializer import PassengerSerializer


class RideSerializer(serializers.ModelSerializer):
    driver = UserSerializer(read_only=True)
    passengers = PassengerSerializer(many=True, read_only=True)
    intermediate = IntermediateSerializer(many=True, read_only=True)

    class Meta:
        model = Ride
        fields = [
            'id',
            'date',
            'point_a',
            'intermediate',
            'point_b',
            'price',
            'seat',
            'vehicle',
            'note',
            'smoking_salon',
            'animals',
            'kids_seat',

            'driver',
            'passengers',
        ]


# TODO
# PrimaryKeyRelatedField(
#         queryset=City.objects
#     )
class RideCreateSerializer(serializers.Serializer):
    date = serializers.DateTimeField(write_only=True)
    point_a = serializers.CharField(write_only=True)
    # intermediate = IntermediateSerializer(many=True, read_only=True)
    point_b = serializers.CharField(write_only=True)
    price = serializers.DecimalField(max_digits=10, decimal_places=2)
    seat = serializers.IntegerField()
    note = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    smoking_salon = serializers.BooleanField(default=False)
    animals = serializers.BooleanField(default=False)
    kids_seat = serializers.BooleanField(default=False)

    def create(self, validated_data):
        driver = self.context['request'].user

        create_ride = usecases.RideCreateUseCase()
        instance = create_ride(driver=driver, **validated_data)
        return instance

    def validate(self, validated_data):
        date = validated_data['date']
        seat = validated_data['seat']
        date_now = timezone.now().date()
        is_valid_date = date.date() < date_now

        if seat <= 0:
            raise ValidationError(detail={"seat": _('Unable to set less than 0')})

        if is_valid_date:
            raise ValidationError(detail={"date": _('Unable to set past date')})

        return validated_data


class RideRentSerializer(serializers.Serializer):
    seat = serializers.IntegerField(write_only=True)

    def validate(self, attrs):
        if attrs['seat'] == 0:
            raise ValidationError(detail={'seat': _('Incorrect count. Count must be more than 0')})
        return attrs

    def update(self, instance, validated_data):
        user = self.context['request'].user
        rent_seat = usecases.RentSeatUseCase()
        instance = rent_seat(
            ride=instance,
            user=user,
            seat=validated_data['seat']
        )
        return instance


class RideRejectRentSerializer(serializers.Serializer):
    seat = serializers.IntegerField(write_only=True)

    def validate(self, attrs):
        if attrs['seat'] == 0:
            raise ValidationError(detail={"seat": _('Incorrect count. Count must be more than 0')})
        return attrs

    def update(self, instance, validated_data):
        user = self.context['request'].user
        rent_seat = usecases.RejectRentSeatUseCase()
        instance = rent_seat(
            ride=instance,
            user=user,
            seat=validated_data['seat']
        )
        return instance
