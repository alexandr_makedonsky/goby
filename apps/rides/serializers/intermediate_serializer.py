from rest_framework import serializers
from apps.rides.models import Intermediate


class IntermediateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Intermediate
        fields = [
            'id',
            'point',
        ]
