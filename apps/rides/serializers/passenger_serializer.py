from rest_framework import serializers

from apps.rides.models import Passenger
from apps.users.serializers import UserPassSerializer


class PassengerSerializer(serializers.ModelSerializer):
    user = UserPassSerializer(read_only=True)

    class Meta:
        model = Passenger
        fields = [
            'user',
            'seat',
        ]
