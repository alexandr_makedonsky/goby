from django.core.management.base import BaseCommand
import csv
from apps.users.models import User
from django_dynamic_fixture import G


class Command(BaseCommand):
    help = 'Create Admin'

    def handle(self, *args, **kwargs):
        admin = G(User,
                  name='admin',
                  phone='375297977468',

                  is_active=True,
                  is_staff=True,
                  is_superuser=True)

        admin.set_password('admin')
        admin.save()




