from rest_framework import mixins, viewsets, permissions
from rest_framework.response import Response

from apps.users.models import Car
from apps.users.serializers import CarSerializer, CarCreateSerializer


class CarViewSet(viewsets.ModelViewSet):
    """
    Use it for car
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = Car.objects.all()
    # serializer_class = CarSerializer
    serializers = {
        'create': CarCreateSerializer,
        'list': CarSerializer,
        'retrieve': CarSerializer,
        'update': CarSerializer,
    }
    http_method_names = ['get', 'post', 'put']

    def get_serializer_class(self):
        return self.serializers[self.action]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)



# TODO delete carlist
class CarListViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializers = {
        'list': CarSerializer,
        'retrieve': CarSerializer,
    }
    queryset = Car.objects.all()
    http_method_names = ['post', 'put', 'get']

    def get_serializer_class(self):
        return self.serializers[self.action]

    def get_queryset(self):
        return self.queryset
        # return self.queryset.filter(user=self.request.user.id)
