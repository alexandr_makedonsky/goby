from rest_framework import mixins, viewsets, permissions
from rest_framework.decorators import action
from apps.users.models import User
from apps.users.serializers import UserSerializer


class UserViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet
):
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_instance(self):
        return self.request.user

    @action(["get", "put", "patch"], detail=False)
    def me(self, request, *args, **kwargs):

        self.get_object = self.get_instance

        if request.method == "GET":
            return self.retrieve(request, *args, **kwargs)
        elif request.method == "PUT":
            return self.update(request, *args, **kwargs)
        elif request.method == "PATCH":
            return self.partial_update(request, *args, **kwargs)
