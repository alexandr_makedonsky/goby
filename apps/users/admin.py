from django.contrib import admin
from apps.users.models import User, Car


admin.site.register(User)
admin.site.register(Car)
