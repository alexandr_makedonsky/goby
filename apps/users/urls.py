from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.users import viewsets, views
from django.conf import settings


router = DefaultRouter()
router.register('users', viewsets.UserViewSet, basename='UserViewSet')
router.register('cars', viewsets.CarViewSet, basename='CarViewSet')


urlpatterns = [
    path('auth/register/', views.UserCreateView.as_view(), name='user-register'),
    path('', include(router.urls)),

]
