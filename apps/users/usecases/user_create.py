import re
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError
from apps.users.models import User


class UserCreateUseCase:
    def __call__(self, name: str, phone: str) -> User:
        self.validate(phone)

        user = User.objects.create(name=name, phone=phone)

        return user

    def validate(self, phone):
        phone_pattern = re.compile('^(\+375|80)(29|25|44|33)?(\d{7})$')
        if not phone_pattern.match(phone):
            raise ValidationError(detail={"phone": _('Phone format is incorrect')})
