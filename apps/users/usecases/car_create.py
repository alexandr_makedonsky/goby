import re
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError
from apps.users.models import User, Car


class CarCreateUseCase:
    def __call__(self, user: User, brand: str, plate: str, pic: str) -> Car:

        car = Car.objects.create(
            user=user, brand=brand, plate=plate, pic=pic)

        return car

    # TODO plate validation
    # def validate(self, plate):
    #     plate_pattern = re.compile('^$')
    #     if not plate.match(plate):
    #         raise ValidationError(detail={"plate": _('Plate is incorrect')})
