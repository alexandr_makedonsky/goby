from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from apps.users.models import User
from apps.users.serializers import UserCreateSerializer


class UserCreateView(generics.CreateAPIView):
    """
    Use this endpoint to register new user.
    """
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs) -> Response:
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)

        return Response(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)
