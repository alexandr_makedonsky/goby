from rest_framework import serializers
from apps.users.models import User
from .car_serializer import CarSerializer


class UserSerializer(serializers.ModelSerializer):
    cars = CarSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'phone',
            'avatar_file',
            'cars',

        ]


class UserPassSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'phone',
            'avatar_file',
        ]
