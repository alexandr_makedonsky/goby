from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User
from apps.users import usecases


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'name',
            'phone',
        ]

    def create(self, validated_data):
        if len(validated_data['name']) < 2:
            raise ValidationError(detail={"name": _('Name is too short')})

        create_user = usecases.UserCreateUseCase()

        return create_user(name=validated_data['name'], phone=validated_data['phone'])
