from rest_framework import serializers
from apps.users.models import Car
from apps.users import usecases


class CarSerializer(serializers.ModelSerializer):
    pic = serializers.ImageField(required=False, allow_null=True)

    class Meta:
        model = Car
        fields = [
            'id',
            'user',
            'brand',
            'plate',
            'pic',
        ]


class CarCreateSerializer(CarSerializer):
    brand = serializers.CharField(required=True)
    plate = serializers.CharField(required=True)
    pic = serializers.ImageField(required=False, allow_null=True)

    def create(self, validated_data):
        user = self.context['request'].user
        # user = self.validated_data['user']
        brand = self.validated_data['brand']
        plate = self.validated_data['plate']
        pic = self.validated_data['pic']

        car_create = usecases.CarCreateUseCase()
        instance = car_create(
            user=user, brand=brand, plate=plate, pic=pic)

        return instance
