from django.db import models
from .user import User
from django.core.validators import MinLengthValidator
from django.utils.translation import ugettext_lazy as _


class Car(models.Model):
    user = models.ForeignKey(User, related_name='cars', on_delete=models.CASCADE)
    brand = models.CharField(
        max_length=16, validators=[MinLengthValidator(2)],
    )
    plate = models.CharField(
        max_length=8, validators=[MinLengthValidator(4)]
    )
    pic = models.ImageField(
        upload_to='cars/', null=True, blank=True, verbose_name=_('cars file')
    )

    def __str__(self):
        return f'{self.user} - {self.brand} - {self.plate}'

    class Meta:
        verbose_name = 'Car'
        verbose_name_plural = 'Cars'

