from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinLengthValidator


class UserManager(BaseUserManager):

    def _create_user(self, phone, **extra_fields):
        user = self.model(phone=phone, **extra_fields)
        user.set_unusable_password()
        user.save(using=self._db)
        return user

    def create_user(self, phone, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)

        return self._create_user(phone, **extra_fields)

    def create_superuser(self, phone, **extra_fields):
        return self._create_user(phone, **extra_fields)

    def get_user_by_token(self, payload):
        try:
            return self.get(id=payload['sub'])
        except self.model.DoesNotExist:
            pass


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(
        max_length=255, validators=[MinLengthValidator(2)], verbose_name=_('name')
    )
    phone = models.CharField(
        max_length=255, unique=True, verbose_name=_('phone')
    )
    avatar_file = models.ImageField(
        upload_to='avatars/', null=True, blank=True, verbose_name=_('avatar file')
    )
    is_staff = models.BooleanField(
        verbose_name=_('staff status'), default=False
    )
    is_active = models.BooleanField(
        verbose_name=_('active'), default=True,
    )
    objects = UserManager()

    EMAIL_FIELD = 'phone'
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return f'{self.name} - {self.phone} '
