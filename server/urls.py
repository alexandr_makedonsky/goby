from django.urls import path, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Poputka API')
schema_view.cls.schema = None

api = [
    path('users/', include(('apps.users.urls', 'urls'), namespace='users')),
    path('', include(('apps.rides.urls', 'urls'), namespace='rides')),


]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view),
    path('v1/', (api, 'api', 'api')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
